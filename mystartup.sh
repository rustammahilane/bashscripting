# startup script for awesomeWM
# to be saved as: /home/mahilane/mystartup.sh
# requirements : fortune-mod, screen-60hz-1920x1080

# set screen resolution to 60hz
xrandr -s 1920x1080 -r 60;

welcome(){
    date;
    printf "Dear $USER,\n";
    printf "The best present life can give you is working hard on work worth doing.\n\n";
    
    fastfetch;

    echo "Today's fortune:";
    randomFortune;
}

randomFortune(){
    # desired set of fortune cookies
    local fortune_set=("art" "education" "bofh-excuses"
    "computers" "humorix-misc" 
    "knghtbrd" "law" "linux" 
    "miscellaneous" "osfortune" "platitudes" 
    "politics" "definitions" "science" 
    "shlomif-fav" "songs-poems" "kernelnewbies"
    "tao" "wisdom" "work") 

	day_of_week=$(date +"%w");
	temp=$((day_of_week * 3));
	random_index=$((RANDOM % 3 + temp));
	random_fortune=${fortune_set[random_index]};
    echo ${fortune_set[random_index]};
    fortune $random_fortune -c;
}

export -f randomFortune;
export -f welcome;
kitty --hold sh -c "welcome;"

unset randomFortune;
unset welcome;
